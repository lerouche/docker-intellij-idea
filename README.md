# docker-intellij-idea

A Docker image for IntelliJ IDEA.

## Features

- The base image is the latest version of Ubuntu.
- IntelliJ IDEA state, specifically `idea.config.path` and `idea.system.path`, are stored in a volume.
- The latest versions of OpenJDK and Node.js are installed.

## Running

Run `docker-run.sh` with 3 arguments, all providing paths to real folders on the host machine
(substitute the braced words with their real values):

|Argument|Description|
|---|---|
|--state {dir}|Where to store the state.|
|--projects {dir}|Where the projects are located.|
|--shared {dir}|Where a folder for sharing files between the host and container is located.|

## Building

You can build using the `docker-build.sh` script.

### User

A user called `developer` with a UID of 1000, group of `developer` (GID 1000), and password `developer`
is by default created in the container, so make sure the folders are owned by 1000:1000.
To customise these, rebuild using `docker-build.sh` with these arguments
(any not provided will be set to their default value):

|Argument|Default value|
|---|---|
|--username {username}|developer|
|--password {password}|developer|
|--group {group}|developer|
|--uid {uid}|1000|
|--gid {gid}|1000|
