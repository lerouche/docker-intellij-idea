#!/usr/bin/env bash

set -e

username="developer"
password="developer"
group="developer"
uid=1000
gid=1000

while [[ $# -gt 0 ]]; do

  key="$1"

  case ${key} in
    --username)
      username="$2"
      shift
      shift
    ;;

    --password)
      password="$2"
      shift
      shift
    ;;

    --group)
      group="$2"
      shift
      shift
    ;;

    --uid)
      uid="$2"
      shift
      shift
    ;;

    --gid)
      gid="$2"
      shift
      shift
    ;;

    *)
      echo "Unknown argument $1"
      exit 1
    ;;
  esac

done

pushd "$(dirname "$0")"

docker build \
 --build-arg username="$username" \
 --build-arg password="$password" \
 --build-arg group="$group" \
 --build-arg uid="$uid" \
 --build-arg gid="$gid" \
 -t wilsonzlin/docker-intellij-idea .

popd

exit 0
