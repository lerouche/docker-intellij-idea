FROM ubuntu:17.10

ARG _distro=artful
ARG _node_repo=node_9.x
ARG username
ARG password
ARG group
ARG uid
ARG gid
ARG _home=/home/${username}
ARG _idea_config_dir=${_home}/.config/IntelliJ-IDEA
ARG _idea_properties=${_idea_config_dir}/idea.properties
ARG _idea64_vm_options=${_idea_config_dir}/idea64.vmoptions
ARG _idea_state_config_dir=${_idea_config_dir}/state/config
ARG _idea_state_system_dir=${_idea_config_dir}/state/system

ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN echo 'Installing OS dependencies' && \
    apt update && \
    apt dist-upgrade -y && \
    apt install -y sudo git wget apt-transport-https default-jdk openjfx && \
    wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
    echo "deb https://deb.nodesource.com/${_node_repo} ${_distro} main" > /etc/apt/sources.list.d/nodesource.list && \
    echo "deb-src https://deb.nodesource.com/${_node_repo} ${_distro} main" >> /etc/apt/sources.list.d/nodesource.list && \
    apt update && \
    apt install -y nodejs && \
    apt autoremove -y && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/*

RUN echo "Creating user: ${username}" && \
    groupadd --gid "${gid}" "${group}" && \
    useradd --create-home --gid "${gid}" --groups sudo --uid "${uid}" "${username}" && \
    echo "${username}:${password}" | chpasswd

RUN echo 'Downloading IntelliJ IDEA' && \
    wget -q https://download.jetbrains.com/idea/ideaIU-2018.1.tar.gz -O /tmp/intellij.tar.gz && \
    echo 'Installing IntelliJ IDEA' && \
    mkdir -p /opt/IntelliJ-IDEA && \
    tar -xf /tmp/intellij.tar.gz --strip-components=1 -C /opt/IntelliJ-IDEA && \
    rm /tmp/intellij.tar.gz

USER $uid

ENV IDEA_PROPERTIES ${_idea_properties}
ENV IDEA64_VM_OPTIONS ${_idea64_vm_options}

RUN mkdir -p "${_idea_state_config_dir}" && \
    mkdir -p "${_idea_state_system_dir}" && \
    echo '\
idea.config.path='${_idea_state_config_dir}'\n\
idea.system.path='${_idea_state_system_dir}'\n' > "${_idea_properties}" && \
    echo '\
-Xms2048m \n\
-Xmx2048m \n\
-XX:NewSize=512m \n\
-XX:MaxNewSize=512m  \n\
-XX:+UseParNewGC \n\
-XX:ParallelGCThreads=4 \n\
-XX:ConcGCThreads=4 \n\
-XX:MaxTenuringThreshold=1 \n\
-XX:SurvivorRatio=8 \n\
-XX:+UseCodeCacheFlushing \n\
-XX:+UseConcMarkSweepGC \n\
-XX:+AggressiveOpts \n\
-XX:+CMSClassUnloadingEnabled \n\
-XX:+CMSIncrementalMode \n\
-XX:+CMSIncrementalPacing \n\
-XX:+CMSParallelRemarkEnabled \n\
-XX:CMSInitiatingOccupancyFraction=65 \n\
-XX:+CMSScavengeBeforeRemark \n\
-XX:+UseCMSInitiatingOccupancyOnly \n\
-XX:ReservedCodeCacheSize=256m \n\
-XX:-TraceClassUnloading \n\
-XX:+AlwaysPreTouch \n\
-XX:+TieredCompilation \n\
-XX:+UseCompressedOops \n\
-XX:SoftRefLRUPolicyMSPerMB=50 \n\
-ea \n\
-Dsun.io.useCanonCaches=false \n\
-Djava.net.preferIPv4Stack=true \n\
-XX:+HeapDumpOnOutOfMemoryError \n\
-XX:-OmitStackTraceInFastThrow \n\
-Dawt.useSystemAAFontSettings=lcd \n\
-Dsun.java2d.renderer=sun.java2d.marlin.MarlinRenderingEngine \n' > "${_idea64_vm_options}"

WORKDIR $_home
ENTRYPOINT /opt/IntelliJ-IDEA/bin/idea.sh
