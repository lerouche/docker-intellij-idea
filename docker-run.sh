#!/usr/bin/env bash

set -e

state_dir=""
projects_dir=""
shared_dir=""

while [[ $# -gt 0 ]]; do

  key="$1"

  case ${key} in
    --state)
      state_dir="$(realpath "$2")"
      shift
      shift
    ;;

    --projects)
      projects_dir="$(realpath "$2")"
      shift
      shift
    ;;

    --shared)
      shared_dir="$(realpath "$2")"
      shift
      shift
    ;;

    *)
      echo "Unknown argument $1"
      exit 1
    ;;
  esac

done

docker run -d \
 -e DISPLAY=${DISPLAY} \
 -v /tmp/.X11-unix:/tmp/.X11-unix \
 -v "$projects_dir":/home/developer/IdeaProjects \
 -v "$state_dir":/home/developer/.config/IntelliJ-IDEA/state \
 -v "$shared_dir":/home/developer/Shared \
 wilsonzlin/docker-intellij-idea

exit 0
